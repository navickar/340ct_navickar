import React from "react";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import { connect } from "react-redux";
//import 'bootstrap/dist/css/bootstrap.min.css';
import ProtectedRoute from "./Components/ProtectedRoute";
import Home from "./Components/Home";
import Login from "./Components/Login";
import Register from "./Components/Register";
import NavBar from './Components/navbar';
import Dashboard from './Components/Dashboard';
import Hits from './Components/Hits';

function App(props) {
  const { isAuthenticated, isVerifying } = props;
  return (

    <div className="App">
        <NavBar fixed="top"/>
        <Router>
        <Switch>
        <ProtectedRoute
          exact
          path="/dashboard"
          component={Dashboard}
          isAuthenticated={isAuthenticated}
          isVerifying={isVerifying}
        />
        <Route path="/login" component={Login} />
        <Route path="/hits" component={Hits}/>
        <Route path="/register" component={Register}/>
        <Route path="/" component={Home}/>
    </Switch>
    </Router>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    isVerifying: state.auth.isVerifying
  };
}export default connect(mapStateToProps)(App);
