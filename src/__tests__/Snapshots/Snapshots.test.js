import React from 'react';
import ReactDOM from 'react-dom';
import Hits from '../../Components/Hits';
import Home from '../../Components/Home';
import Register from '../../Components/Register';
import Login from '../../Components/Login';
import configureStore from "../../configureStore";
import App from "../../App";
import Dashboard from '../../Components/Dashboard';

const store = configureStore();

describe('Snapshot tests:', () =>{
    test('Hits Snapshot', () => {
        const hits = document.createElement('hits');
        ReactDOM.render(<Hits store = {store}/>,hits);
        ReactDOM.unmountComponentAtNode(hits);

    });
    test('Home Snapshot', () => {
        const home = document.createElement('home');
        ReactDOM.render(<Home store = {store}/>,home);
        ReactDOM.unmountComponentAtNode(home);
    });

    test('Register Snapshot', () => {
        const reg = document.createElement('reg');
        ReactDOM.render(<Register store={store}/>,reg);
        ReactDOM.unmountComponentAtNode(reg);
    });
    test('Login Snapshot', () => {
        const login = document.createElement('login');
        ReactDOM.render(<Login store={store}/>,login);
        ReactDOM.unmountComponentAtNode(login);
    });

    test('App Snapshot', () => {
        const div = document.createElement('div');
        ReactDOM.render(<App store={store}/>,div);
        ReactDOM.unmountComponentAtNode(div);
    });
    test('Dashboard Snapshot', () => {
        const dash = document.createElement('dash');
        ReactDOM.render(<Dashboard store={store}/>,dash);
        ReactDOM.unmountComponentAtNode(dash);

    });
});