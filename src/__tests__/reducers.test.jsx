import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGOUT_REQUEST,
    LOGOUT_SUCCESS,
    LOGOUT_FAILURE,
    VERIFY_REQUEST,
    VERIFY_SUCCESS
  } from "../__db__/actions";


describe('reducers:', () => {
    test('should assign correct state according to action', () => {

        expect(LOGIN_REQUEST).toReturn
        expect(LOGIN_SUCCESS).toReturn
        expect(LOGIN_FAILURE).toReturn
        expect(LOGOUT_REQUEST).toReturn
        expect(LOGOUT_SUCCESS).toReturn
        expect(LOGOUT_FAILURE).toReturn
        expect(VERIFY_REQUEST).toReturn
        expect(VERIFY_SUCCESS).toReturn
    }); 
});
