import configureStore from "../configureStore";
import Dashboard from '../Components/Dashboard';
import React from "react";
const store = configureStore();
const DashProps = (props) => <Dashboard store={store} {...props}/>;
jest.mock('../Components/Dashboard', () => {
    return jest.fn().mockImplementation(() => {
        return {handleLgt: () => {
        }};

    });
});


let dash = new Dashboard();
describe("Dashboard", () =>{
    it('Test Logout func:', () =>{
        expect(dash.handleLgt()).toHaveBeencalled;
    });
    it('Test Logout run:', () =>{
        expect(dash.handleLgt()).anything;
    })
});