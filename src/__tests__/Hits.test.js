import Hits from '../Components/Hits';

jest.mock('../Components/Hits', () => {
    return jest.fn().mockImplementation(() => {
        return {formatDt: (date) => {
            var time = new Date(date);
            var year = time.getFullYear();
            var day = time.getDate();
            var hour = time.getHours();
            var minute = time.getMinutes();
            var month = time.getMonth() + 1;
            var composedTime = day + '/' + month + '/' + year + ' | ' + hour + ':' + (minute < 10 ? '0' + minute : minute);
            return composedTime;
        }};
    });
});


let hits = new Hits();
describe("Hits", () =>{
    it('DateTests:', () =>{
        const date = '2019-11-23T22:15:00Z';
        expect(hits.formatDt(date)).toEqual('23/11/2019 | 22:15');
    });
});