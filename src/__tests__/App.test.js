import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';
import configureStore from "../configureStore";


const store = configureStore();

describe('App.js tests:', () => {
    test('props', () =>{
        const AppProps = (props) => <App store={store} {...props}/>;
       expect(AppProps()).anything;
       expect(true).toBeTruthy();
        
    });
});
