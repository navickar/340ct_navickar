
import React from 'react'
import {Navbar, Nav, Button, FormControl, Form} from 'react-bootstrap'



import { withStyles } from "@material-ui/styles";


//Google Icon imports
import DashboardOutlinedIcon from '@material-ui/icons/DashboardOutlined';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import WhatshotOutlinedIcon from '@material-ui/icons/WhatshotOutlined';
import LockOpenOutlinedIcon from '@material-ui/icons/LockOpenOutlined';
import AccountBoxOutlinedIcon from '@material-ui/icons/AccountBoxOutlined';








const styles = () => ({
  "@global": {
    body: {
      backgroundColor: "#fff"
    }
  },
  paper: {
    marginTop: 100,
    display: "flex",
    padding: 10,
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    marginLeft: "auto",
    marginRight: "auto",
    backgroundColor: "#343a40"
  },
  form: {
    marginTop: 1
  },
});




function NavBar() {
  return (
    <div className="navbar-nav">
<Navbar bg="dark" variant="dark">
    <Navbar.Brand href="/"><HomeOutlinedIcon/></Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Link href="/hits"><WhatshotOutlinedIcon/></Nav.Link>
      <Nav.Link href="/login"><AccountBoxOutlinedIcon/></Nav.Link>
      
    </Nav>
    <Form inline>
      <FormControl type="text" placeholder="Search" className="mr-sm-2" />
      <Button variant="outline-info"><SearchOutlinedIcon/></Button>
    </Form>
    <Nav className="mav justify-content-end">
    <Nav.Link href="/register"><LockOpenOutlinedIcon/></Nav.Link>
    <Nav.Link href="/dashboard"><DashboardOutlinedIcon/></Nav.Link>
    </Nav>
  </Navbar>

    </div>
  );
}

export default withStyles(styles)(NavBar);
