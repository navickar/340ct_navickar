import React, { Component } from 'react';
import axios from 'axios';
import Carousel from 'react-bootstrap/Carousel'
import { withStyles } from "@material-ui/styles";



const styles = () => ({
    "@global": {
      body: {
        backgroundColor: "#e8e8e8"
      }
    },
    paper: {
      marginTop: 100,
      display: "flex",
      padding: 20,
      flexDirection: "column",
      alignItems: "center",
      backgroundColor: "#e8e8e8"
    },
    form: {
      marginTop: 1
    },
    frameStyle: {
        backgroundColor: "#111",
        paddingBottom: "10"
    },
    CarouselStyle: {
        align:"centre",
    }
  });

class Home extends Component {
    constructor(props) {
      // Pass props to parent class
      super(props);
      // Set initial state
      this.state = {
        articles: []
      };
    }
  
    // Lifecycle method
    UNSAFE_componentWillMount() {
      this.getArticles(this.props.default);
    }
  
    UNSAFE_componentWillReceiveProps(nextProps) {
      if (nextProps !== this.props) {
        this.setState({ url: `https://newsapi.org/v2/top-headlines?country=uk&apiKey=dedc97886ba34eec97a82f5d7979703f` });
  
        this.getArticles(nextProps.default);
      }
    }
  
    formatDate(date) {
      var time = new Date(date);
      var year = time.getFullYear();
      var day = time.getDate();
      var hour = time.getHours();
      var minute = time.getMinutes();
      var month = time.getMonth() + 1;
      var composedTime = day + '/' + month + '/' + year + ' | ' + hour + ':' + (minute < 10 ? '0' + minute : minute);
      return composedTime;
    }
  
    getArticles(url) {
      const apiKey = process.env.dedc97886ba34eec97a82f5d7979703f;
      // Make HTTP reques with Axios
      axios
        .get(`https://newsapi.org/v2/top-headlines?country=us&apiKey=dedc97886ba34eec97a82f5d7979703f`)
        .then(res => {
          const articles = res.data.articles;
          // Set state with result
          console.log(articles);
          this.setState({ articles: articles });
        })
        .catch(error => {
          console.log(error);
        });
    }
  
    render() {
      return (
        <Carousel className={styles.CarouselStyle}>
          {this.state.articles.map((news, i) => {
            return (
            <Carousel.Item width = '90%' align='center'>
                <a href={news.url}>
                <img  width = '80%%' src={news.urlToImage}
                alt={news.title}/>
                </a>
                <Carousel.Caption>
                    <h5 >
                    {news.title}
                    </h5>
                    <p></p>
                    <p>{news.description}</p>
                </Carousel.Caption>

            </Carousel.Item>            
            );
          })}
        </Carousel>
      );
    }
  }
  
  export default withStyles(styles)(Home);