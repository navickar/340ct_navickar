import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect} from "react-router-dom";
import { loginUser } from "../__db__/actions";
import { withStyles } from "@material-ui/styles";


//Google Icon imports
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";

const styles = () => ({
  "@global": {
    body: {
      backgroundColor: "#fff"
    }
  },
  paper: {
    marginTop: 100,
    display: "flex",
    padding: 20,
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "#e8e8e8"
  },
  avatar: {
    marginLeft: "auto",
    marginRight: "auto",
    backgroundColor: "#f50057"
  },
  form: {
    marginTop: 1
  },
  errorText: {
    color: "#f50057",
    marginBottom: 5,
    textAlign: "center"
  },
  TextField: {
    backgroundColor: "#fff"
  }
});


class Login extends Component {
    state = { email: "", password: "", isRegisterClicked: false };
  
    handleEmailChange = ({ target }) => {
      this.setState({ email: target.value });
    };
  
    handlePasswordChange = ({ target }) => {
      this.setState({ password: target.value });
    };
  
    handleSingInSubmit = () => {
      const { dispatch } = this.props;
      const { email, password } = this.state;
      try{
        if(email === ""){
          alert("Email cannot be empty!");
          throw("Email cannot be empty!");
        }
        else if(password === ""){
          alert("Password cannot be empty!");
          throw("Password cannot be empty!");
        }
      }catch(e){
          throw(e);
        }
  
      dispatch(loginUser(email, password));
    };


    handleRegisterSubmit = () => {
      this.setState({isRegisterClicked : true})
      console.log(this.state.isRegisterClicked);
      }
  
    render() {
      const { classes, loginError, isAuthenticated } = this.props;
      if(this.state.isRegisterClicked){
        return <Redirect to="/register" />;
      }
      else if (isAuthenticated) {
        return <Redirect to="/dashboard" />;
      } else {
        return (
          <Container component="main" maxWidth="xs">
            <Paper className={classes.paper}>
              <Typography component="h2" variant="h6">
                In order to enter - you need to login!
              </Typography>
              <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign in
              </Typography>
              <TextField
                className={classes.TextField}
                variant="outlined"
                margin="normal"
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                onChange={this.handleEmailChange}
              />
              <TextField
                className={classes.TextField}
                variant="outlined"
                margin="normal"
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                onChange={this.handlePasswordChange}
              />
              {loginError && (
                <Typography component="p" className={classes.errorText}>
                  Incorrect email or password.
                </Typography>
              )}
              <Button
                name="Login"
                type="button"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={this.handleSingInSubmit}
              >
                Sign In
              </Button>
              &nbsp;
              <Button
                name="Register"
                type="button"
                fullWidth
                variant="contained"
                color="secondary"
                onClick={this.handleRegisterSubmit}
              >
                Register
              </Button>
            </Paper>
          </Container>
        );
      }
    }
  }
  
  function mapStateToProps(state) {
    return {
      loginError: state.auth.loginError,
      isAuthenticated: state.auth.isAuthenticated
    };
  }

  export default withStyles(styles)(connect(mapStateToProps)(Login));