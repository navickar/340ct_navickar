import React, { Component } from "react";
import { connect } from "react-redux";
import { registerUser } from "../__db__/actions";
import { withStyles } from "@material-ui/styles";


//Google Icon imports
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import LockOpenOutlinedIcon from "@material-ui/icons/LockOpenOutlined";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";


const styles = () => ({
  "@global": {
    body: {
      backgroundColor: "#fff"
    }
  },
  paper: {
    marginTop: 100,
    display: "flex",
    padding: 20,
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "#e8e8e8"
  },
  avatar: {
    marginLeft: "auto",
    marginRight: "auto",
    backgroundColor: "#f50057"
  },
  form: {
    marginTop: 1
  },
  errorText: {
    color: "#f50057",
    marginBottom: 5,
    textAlign: "center"
  },
  TextField: {
    backgroundColor: "#fff"
  }
});


class Register extends Component {
    


    handleEmailChange = ({ target }) => {
        this.setState({ email: target.value });
      };
    
    handleFirstNameChange = ({target}) =>{
        this.setState({name: target.value});
    };
    handleFirstNameChange = ({target}) =>{
      this.setState({surname: target.value});
  };
    
    handlePasswordChange = ({ target }) => {
        this.setState({ password: target.value });
    };
    state = { name: "", surname: "",email: "", password: "" };
    handleRegisterSubmit =  () => {
        const { dispatch } = this.props;
        const { email, password, name, surname} = this.state;
        try{
          if(email === ""){
            alert("Email cannot be empty!");
            throw("Email cannot be empty!");
          }
          else if(password === ""){
            alert("Password cannot be empty!");
            throw("Password cannot be empty!");
          }
          else if (name === ""){
            alert("Name cannot be empty!");
            throw("Name cannot be empty!")
          }
          else if (surname === ""){
            alert("Surname cannot be empty!");
            throw("Surname cannot be empty!")
          }
        }catch(e){
          throw e;
        }
    
        dispatch(registerUser(email, password));
      };
 
    render() {
      const { classes, registerError, isRegistered} = this.props;
        return (
          <Container component="main" maxWidth="xs">
            <Paper className={classes.paper}>
              <Typography component="h2" variant="h6">
              </Typography>
              <Avatar className={classes.avatar}>
                <LockOpenOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Register
              </Typography>
              <TextField
                className={classes.TextField}
                variant="outlined"
                margin="normal"
                fullWidth
                id="name"
                label="First Name"
                name="name"
                onChange={this.handleFirstNameChange}
              />
              <TextField
                className={classes.TextField}
                variant="outlined"
                margin="normal"
                fullWidth
                id="surname"
                label="Last Name"
                name="surname"
                onChange={this.handleSurnameChange}
              />
              <TextField
                className={classes.TextField}
                variant="outlined"
                margin="normal"
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                onChange={this.handleEmailChange}
              />
              <TextField
                className={classes.TextField}
                variant="outlined"
                margin="normal"
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                onChange={this.handlePasswordChange}
              />
              &nbsp;
              &nbsp;
              {registerError && (
                <Typography component="p" className={classes.errorText}>
                  Something went wrong, we couldn't create your account. Please try again.
                </Typography>
              )}
              {isRegistered &&(
                  <Typography component="p" className={classes.errorText}>
                  Email verification was sent, please check your email.
                </Typography>
              )}
              <Button
                type="button"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={this.handleRegisterSubmit}
              >
                Register
              </Button>
            </Paper>
          </Container>
        );
      }
    }

function mapStateToProps(state) {
    return {
      registerError: state.auth.registerError,
      isRegistered: state.auth.isRegistered
    };
  }

  export default withStyles(styles)(connect(mapStateToProps)(Register));