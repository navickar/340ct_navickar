import React, { Component } from 'react';
import axios from 'axios';
import { withStyles } from "@material-ui/styles";

//Google Icon imports
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";

const styles = () => ({
    "@global": {
      body: {
        backgroundColor: "#e8e8e8"
      }
    },
    paper: {
      marginTop: 100,
      display: "flex",
      padding: 20,
      flexDirection: "column",
      alignItems: "center",
      backgroundColor: "#e8e8e8"
    },
    form: {
      marginTop: 1
    },
    frameStyle: {
        backgroundColor: "#111",
        paddingBottom: "10"
    },
    CarouselStyle: {
        width:"100%"
    }
  });

class Hits extends Component {
    constructor(props) {
      // Pass props to parent class
      super(props);
      // Set initial state
      this.state = {
        articles: []
      };
    }
  
    // Lifecycle method
    UNSAFE_componentWillMount() {
      this.getArticles(this.props.default);
    }
  
    UNSAFE_componentWillReceiveProps(nextProps) {
      if (nextProps !== this.props) {
        this.setState({ url: `https://newsapi.org/v2/everything?q=bitcoin&apiKey=dedc97886ba34eec97a82f5d7979703f` });
  
        this.getArticles(nextProps.default);
      }
    }
  
    formatDate(date){
      var time = new Date(date);
      var year = time.getFullYear();
      var day = time.getDate();
      var hour = time.getHours();
      var minute = time.getMinutes();
      var month = time.getMonth() + 1;
      var composedTime = day + '/' + month + '/' + year + ' | ' + hour + ':' + (minute < 10 ? '0' + minute : minute);
      return composedTime;
    }
  
    getArticles(url) {
      const apiKey = process.env.dedc97886ba34eec97a82f5d7979703f;
      // Make HTTP reques with Axios
      axios
        .get(`https://newsapi.org/v2/everything?q=bitcoin&apiKey=dedc97886ba34eec97a82f5d7979703f`)
        .then(res => {
          const articles = res.data.articles;
          // Set state with result
          this.setState({ articles: articles });
        })
        .catch(error => {
        });
    }
  
    render() {
      return (
        <Container component="main" maxWidth="sm">
          {this.state.articles.map((news, i) => {
            return (
                <Paper className={styles.paper}>
                <div className="card" >
                  <div className="content" styles= "width :80%">
                  &nbsp;
                  <Typography  className={styles.Typography}>
                    <p>
                      <a href={news.url} target="_blank" rel="noopener noreferrer">
                        {news.title}
                      </a>
                    </p>
                    <p>{news.description}</p>
                    </Typography>
                    <div className="author">
                      <p>
                        By <i>{news.author ? news.author : this.props.default}</i>
                      </p>
                      <p>{this.formatDate(news.publishedAt)}</p>
                    </div>
                  </div>
                  <div>
                    <img src={news.urlToImage} width='400' alt="" />
                  </div>
                  <div>
                  &nbsp;
              </div>
                </div>
                </Paper>
            );
          })}
        </Container>
      );
    }
  }
  
  export default withStyles(styles)(Hits);
  