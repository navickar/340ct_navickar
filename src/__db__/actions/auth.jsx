import { myFirebase } from "../databaseAPI";

export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";

export const LOGOUT_REQUEST = "LOGOUT_REQUEST";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const LOGOUT_FAILURE = "LOGOUT_FAILURE";

export const VERIFY_REQUEST = "VERIFY_REQUEST";
export const VERIFY_SUCCESS = "VERIFY_SUCCESS";

export const SIGNUP_REQUEST = "SIGNUP_REQUEST";
export const SIGNUP_SUCCESS = "SIGNUP_SUCCESS";
export const SIGNUP_ERROR = "SIGNUP_ERROR";

const requestLogin = () => {
  return {
    type: LOGIN_REQUEST
  };
};

const receiveLogin = user => {
  return {
    type: LOGIN_SUCCESS,
    user
  };
};

const loginError = () => {
  return {
    type: LOGIN_FAILURE
  };
};

const requestLogout = () => {
  return {
    type: LOGOUT_REQUEST
  };
};

const receiveLogout = () => {
  return {
    type: LOGOUT_SUCCESS
  };
};

const logoutError = () => {
  return {
    type: LOGOUT_FAILURE
  };
};

const verifyRequest = () => {
  return {
    type: VERIFY_REQUEST
  };
};

const verifySuccess = () => {
  return {
    type: VERIFY_SUCCESS
  };
};
const requestRegister = () => {
  return {
    type: SIGNUP_REQUEST
  };
};
const verifyRegister = () => {
  return {
    type: SIGNUP_SUCCESS,
    payload: "Your account was successfully created! Now you need to verify your e-mail address, please go check your inbox."
  };
};
const registerError = () => {
  return {
    type: SIGNUP_ERROR,
    payload: "Something went wrong, we couldn't create your account. Please try again."
  };
};

export const loginUser = (email, password) => dispatch => {
  dispatch(requestLogin());
  myFirebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(user => {
      dispatch(receiveLogin(user));
    })
    .catch(error => {
      //Do something with the error if you want!
      dispatch(loginError());
    });
};

export const logoutUser = () => dispatch => {
  dispatch(requestLogout());
  myFirebase
    .auth()
    .signOut()
    .then(() => {
      dispatch(receiveLogout());
    })
    .catch(error => {
      //Do something with the error if you want!
      dispatch(logoutError());
      return error;
    });
};

export const verifyAuth = () => dispatch => {
  dispatch(verifyRequest());
  myFirebase
    .auth()
    .onAuthStateChanged(user => {
      if (user !== null) {
        dispatch(receiveLogin(user));
      }
      dispatch(verifySuccess());
    });
};

export const  registerUser = (email, password) => async dispatch =>{
  dispatch(requestRegister());
  try{
    myFirebase
    .auth()
    .createUserWithEmailAndPassword(email, password)
    .then(dataBeforeEmail =>{
      myFirebase
      .auth()
      .onAuthStateChanged(function(user){
        user.sendEmailVerification();
  });
  })
  .then(dataAfterEmail =>{
    myFirebase
    .auth()
    .onAuthStateChanged(function(user){
      if (user.emailVerified){
        dispatch(verifyRegister());
      }
      else{
        dispatch(registerError());
      }
    });
  });
  } catch(err){
    dispatch(registerError());
  }
};
