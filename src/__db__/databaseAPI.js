import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyDTJcb2TSIm4dhe1-vU6eikKrjuFl0Eexw",
    authDomain: "ct-41fc3.firebaseapp.com",
    databaseURL: "https://ct-41fc3.firebaseio.com",
    projectId: "ct-41fc3",
    storageBucket: "ct-41fc3.appspot.com",
    messagingSenderId: "115751509020",
    appId: "1:115751509020:web:8aa6934ef52046fa21d3d9",
    measurementId: "G-WJGJYP73HG"
  };

export const myFirebase = firebase.initializeApp(firebaseConfig);
const baseDb = myFirebase.firestore();
export const db = baseDb;
