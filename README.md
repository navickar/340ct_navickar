### Ensure you have newest version of NodeJS before using.

## To Run the project

* Clone the project
* Navigate to project folder
* run `npm install` to get the node packages installed
* run `npm start` to start the dev-server.

---
